# from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.files.storage import FileSystemStorage
from django.conf import settings
fs=FileSystemStorage(location=settings.MEDIA_ROOT)
class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField("profile image",upload_to='users/',blank=True, storage=fs)

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

# class Product(models.Model):
#     user = models.ForeignKey(Profile)
#     text = models.CharField(blank=True,null=True)
#     image = models.ImageField("product image",upload_to='product/',blank=True,storage=fs)


