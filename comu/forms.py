from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
class LoginForm(forms.Form):
    username = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': 'username'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'password'}))

class SignUpForm(UserCreationForm):
   username = forms.CharField(max_length=30, )
   last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
   first_name = forms.CharField(max_length=30, help_text='Optional.')
   email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
   password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
   password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)
   class Meta:
      model = User
      fields = ('username','first_name','last_name','email','password1', 'password2')
   def clean_password2(self):
      cd = self.cleaned_data
      if cd['password1'] != cd['password2']:
        raise forms.ValidationError('Passwords don\'t match.')
      return cd['password2']


class UserEditForm(forms.ModelForm):
   class Meta:
      model = User
      fields = ('first_name', 'last_name', 'email',)
class ProfileEditForm(forms.ModelForm):
   class Meta:
      model = Profile
      fields = ('date_of_birth', 'photo',)

