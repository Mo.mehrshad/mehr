# from django.http import HttpResponse


from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.template import loader
from .forms import LoginForm
from django.views import generic
from django.views.generic import View
from django.shortcuts import render_to_response,redirect
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, SignUpForm
from .forms import LoginForm, SignUpForm, \
UserEditForm, ProfileEditForm

# class UserFormView(View):
#     form_class=LoginForm
#     template_name='comu/templates/comu/eror.html'
#     print template_name
#     def get(self,request):
#         form=self.form_class(None)
#         return render(request,self.template_name,{'form':form})
#     def post(self,request):
#         form = self.form_class(None)
#         if form.is_valid():
#               user =form.save(commit=False)
#               username=form.cleaned_data['username']
#               password = form.cleaned_data['password']
#               user.set_password(password)
#               user.save()
#               user=authenticate(username=username,password=password)
#               if user is not None:
#                   if user.is_active:
#                       login(request,user)
#                       return redirect('comu:eror')
#         return render(request, self.template_name, {'form': form})
##########################################


@login_required

def dashboard(request):
          return render(request,'dashboard.html',{'section': 'dashboard'})

def index(request):
    template = loader.get_template('registration/login.html')
    return HttpResponse(template.render())

def register(request):
    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        if user_form.is_valid():

             new_user = user_form.save(commit=False)

             new_user.set_password(user_form.cleaned_data['password1'])
             new_user.save()
             return render(request,'register_done.html',{'new_user': new_user})
    else:
       user_form = SignUpForm()
    return render(request,'register.html',{'user_form': user_form})


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(request.POST , instance=request.user)
        profile_form = ProfileEditForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request, 'edit.html', {'user_form': user_form, 'profile_form': profile_form})