from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^loginh/', views.index),
url(r'^login/$','django.contrib.auth.views.login',name='login'),
url(r'^logout/$','django.contrib.auth.views.logout',name='logout'),
url(r'^logout-then-login/$','django.contrib.auth.views.logout_then_login',name='logout_then_login'),
url(r'^$', views.dashboard, name='dashboard'),
url(r'^register/$', views.register, name='register'),

url(r'^edit/$', views.edit, name='edit'),

]